import React from "react";
import Login from "./components/Login";
import Signup from "./components/Signup";
import SignInOutContainer from "./containers/index";

const App = () => {
  return (
    <div>
      <SignInOutContainer />
    </div>
  );
};

export default App;
